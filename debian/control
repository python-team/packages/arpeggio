Source: arpeggio
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Philipp Huebner <debalance@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-pytest,
               python3-pytest-runner,
               python3-setuptools,
               python3-wheel
Standards-Version: 4.6.2
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python
Homepage: https://textx.github.io/Arpeggio/
Vcs-Browser: https://salsa.debian.org/python-team/packages/arpeggio
Vcs-Git: https://salsa.debian.org/python-team/packages/arpeggio.git

Package: python3-arpeggio
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Suggests: python-arpeggio-doc
Description: parser interpreter based on PEG grammars (Python 3)
 Arpeggio is a recursive descent parser with memoization based on PEG grammars
 (aka Packrat parser).
 .
 This package installs the library for Python 3.

Package: python-arpeggio-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Multi-Arch: foreign
Description: parser interpreter based on PEG grammars (common documentation)
 Arpeggio is a recursive descent parser with memoization based on PEG grammars
 (aka Packrat parser).
 .
 This is the common documentation package.
